import csv
import requests
import html5lib
from bs4 import BeautifulSoup
from time import sleep
import datetime
import time
# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.firefox.webdriver import WebDriver
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.support.wait import WebDriverWait
#
# DELAY=3
#
# browser: WebDriver = webdriver.Firefox()
# browser.get('https://futabus.vn')
# # print(browser.page_source)
# print(browser.current_url)
# browser.close()



def extract_number(inp_str):
    num = ""
    for c in inp_str:
        if c.isdigit():
            num = num + c
    return num


headers = {'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246"}

token = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImFkMWIxOWYwZjU4ZTJjOWE5Njc3M2M5MmNmODA0NDEwMTc5NzEzMjMiLCJ0eXAiOiJKV1QifQ.eyJhbm9ueW1vdXMiOnRydWUsImlzcyI6Imh0dHBzOi8vc2VjdXJldG9rZW4uZ29vZ2xlLmNvbS9mYWNlY2FyLTI5YWU3IiwiYXVkIjoiZmFjZWNhci0yOWFlNyIsImF1dGhfdGltZSI6MTY2MDUzODA2NCwidXNlcl9pZCI6InNFMkk1dkg3TTBhUkhWdVl1QW9QaXByczZKZTIiLCJzdWIiOiJzRTJJNXZIN00wYVJIVnVZdUFvUGlwcnM2SmUyIiwiaWF0IjoxNjYwNTQxOTk3LCJleHAiOjE2NjA1NDU1OTcsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnt9LCJzaWduX2luX3Byb3ZpZGVyIjoiY3VzdG9tIn19.SXaeBxr2VMKctmZ7lQ8reyvy2KOLW8iBOuomfKohc2q6rAsWem96rDGF5eTB59z-fgkdCPcCV3d1k6CZ8QXnQmgAwyYKZdetIoE-t_AzOiDuRtuPnnieVch2C_un-TyZ4FUnZ38yBQnHViO97d4RaCHs-InE_ph7oZoOQ8-4aYfTs6k1l9GUGBeSyHq-kAkaW0dCt-6Yy_6iEBbJjsLZnTfn97DAbmsAj8iN93MZsgwtWa0f01vytNLAShB8cQHGlbTUMoR_sOJw0VMTzptBCMLdDvrkkgjO4_J7LwX27k3fHQiGKQKWaf2RJaV7aFWGxR6A5qnaHf6uTyQIfJyWmg"
date = "30%2F10%2F2022"
#URL = f"https://futabus.vn/booking/ve-xe-phuong-trang-an-giang-di-tp-ho-chi-minh.html?step=2&from=%257B%2522code%2522%253A%2522CHAUDOC%2522%252C%2522name%2522%253A%2522An%2520Giang%2522%252C%2522keyword%2522%253A%2522%2522%252C%2522type%2522%253A%2522location%2522%257D&to=%257B%2522code%2522%253A%2522TPHCM%2522%252C%2522name%2522%253A%2522TP.H%25u1ED3%2520Ch%25ED%2520Minh%2522%252C%2522keyword%2522%253A%2522SAIGON%2520SAI%2520GON%2520HCM%2522%252C%2522type%2522%253A%2522location%2522%257D&token={token}&bookFromDate={date}&lang=en-US"
URL = "https://futabus.vn/?lang=en-US"
r = requests.get(URL , headers=headers)

soup = BeautifulSoup(r.content, 'html5lib')
quotes = []  # a list to store quotes
# print(soup)

place_outer = soup.findAll("div", attrs={"class":"data-list-container hide left"})
print(place_outer[1])

for place in soup.findAll("button", attrs={"class":"select-place-item list-item"}):
    print(place.text)

table = soup.find('div', attrs={'data-fetch-key': 'data-v-008a65cb:0',"class":"step-2-container"})
head = soup.find('div', attrs={"class":"content"})
print(head.h1.text)
print(head.div.text)

# print("prettify\n" , table.prettify())
i=0
for row in table.findAll('div',
                         attrs={'class': 'route-option'}):
    if i > 2:
        print(row.prettify())
        quote = {}
        quote['Departure'] = "Ben"
        quote['Arrival'] = "Ben2"
        quote['Date'] = "30-09-2022"
        quote['Bus No'] = f"{i}"
        time = row.find( 'div', attrs={"class":"header"}).text.replace("\n","").strip().split()
        quote['Depart time'] = time[0]
        quote['Arrival time'] = time[1]
        fare = row.find("div", attrs={"class":"label"}).text.replace("\n", "").strip().partition(" ")
        quote['Bus Fare'] = fare[0]
        quote['Seat Count'] = extract_number(fare[2])
        location = row.findAll("div", attrs={"class":"route-line bold"})
        quote['Pickup Location'] = location[0].text.split("\n")[1].strip()
        quote['Drop Location'] = location[1].text.strip()

        quotes.append(quote)
    print(i)
    i += 1
    # if i == 5:
    #     break

filename = 'bus_route.csv'
with open(filename, 'w', newline='') as f:
    w = csv.DictWriter(f, ['Departure', 'Arrival', 'Date', 'Bus No', 'Depart time','Arrival time','Bus Fare',
                           'Seat Count', 'Pickup Location', 'Drop Location'])
    w.writeheader()
    for quote in quotes:
        w.writerow(quote)


